# Battery Client with C

## General

The purpose of this project is to control laptop power supply based on data sent to a webserver through websocket communication.
This project involved using an ASUS laptop with Ubuntu 22.04. Operation on other brands, Linux distributions or different versions
of Ubuntu is not guaranteed.

## Installation

libwebsockets
// run:
sudo apt install libwebsockets-dev

jansson
// run:
sudo apt install libjansson-dev

## Startup

Compilation
// export env variables and compile the code
source env.sh && gcc -O2 -Wall -g -o main main.c websocket.c battery.c -lwebsockets -ljansson

Run
./main
