#include <stdio.h>
#include <stdlib.h>
#include "./battery.h"

int main() {
  float battery_level = 0;
  char battery_status[20] = {'\0'};

  int result = get_battery_status(&battery_level, battery_status);

  printf("Level: %.2f%%, Charge: %s\n", battery_level, battery_status);

  return 0;
}
